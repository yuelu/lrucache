package com.netsuite.lruCache;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import org.junit.Test;

public class SimpleLruCacheTest {

    @Test
    public void testNullKeyValue() {

        SimpleLruCache cache = new SimpleLruCache(5);

        try {
            cache.put(null, "");
            fail("should fail - key can't be null");
        } catch (NullPointerException e) {

        }

        try {
            cache.put("", null);
            fail("should fail - value can't be null");
        } catch (NullPointerException e) {

        }

        try {
            cache.put(null, null);
            fail("should fail - key and value can't be null");
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void test() {

        SimpleLruCache cache = new SimpleLruCache(5);

        // cache empty
        assertThat(cache.get("somekey"), nullValue());

        fillCache(cache);
        // key not found
        assertThat(cache.get("somekey"), nullValue());
        assertThat(cache.toString(), is("{key1=value1,key2=value2,key3=value3,key4=value4,key5=value5}"));

        // put a new value for existing key
        cache.put("key1", "newValue1");
        assertThat(cache.toString(), is("{key2=value2,key3=value3,key4=value4,key5=value5,key1=newValue1}"));

        // put a new (key, value) pair, the least recently used key=key2 got removed
        cache.put("key6", "value6");
        assertThat(cache.toString(), is("{key3=value3,key4=value4,key5=value5,key1=newValue1,key6=value6}"));
    }

    private void fillCache(SimpleLruCache cache) {
        for (int i = 1; i <= cache.getMaxSize(); i++)
            cache.put("key" + i, "value" + i);
    }
}
