package com.netsuite.lruCache;

import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * <pre>
 * A Least Recently Used Cache is expected to keep all items that have been used the most recently, and 
 * discard the ones that haven’t been used in the longest amount of time as it becomes full.
 * 
 * Notes
 * 1). “Recently used” is defined as either being retrieved or inserted.
 * 2).In this exercise, assume getMaxSize() will return the maximum number of items that can be 
 * in the cache. We aren’t worrying about memory footprint or anything like that - just the number 
 * of items.
 * 3). While you do not need to implement getMaxSize() for this exercise, you can for testing 
 * purposes if you wish. We will not evaluate this method, so it can be anything you require.
 * 4). The get() method should return null in cases where it does not return an Object.
 * 5). The toString() method should return a string representation of the items in the cache,
 * including keys and values, in order of recent use.
 * 6). Please use any solution you like, but you cannot use a LinkedHashMap in your implementation.
 * 
 * </pre>
 * 
 * @author jlu
 *
 */
public class SimpleLruCache implements LruCache {

    private final int cacheSize;
    private final Queue<Object> keys = new ConcurrentLinkedQueue<>();
    private final Map<Object, Object> cache = new ConcurrentHashMap<>();

    public SimpleLruCache(int cacheSize) {
        this.cacheSize = cacheSize;
    }

    @Override
    public synchronized Object get(Object key) {

        if (keys.contains(key)) {
            keys.remove(key);
            keys.add(key);
        }

        return cache.get(key);
    }

    @Override
    public synchronized void put(Object key, Object value) {

        if (key == null || value == null)
            throw new NullPointerException("key and value should not be null!");

        if (keys.contains(key)) {
            keys.remove(key);
        } else {
            ensureCacheSize();
        }

        keys.add(key);
        cache.put(key, value);
    }

    /**
     * if cache full, remove the least recently used (meaning being retrieved or inserted) key from the cache
     */
    private void ensureCacheSize() {
        if (keys.size() == cacheSize) {
            Object lruKey = keys.poll();
            if (lruKey != null) {
                cache.remove(lruKey);
            }
        }
    }

    @Override
    public int getMaxSize() {
        return cacheSize;
    }

    /**
     * return a string representation of the items in the cache, including keys and values, in order of recent use
     */
    @Override
    public synchronized String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for (Object key : keys) {
            sb.append(key);
            sb.append("=");
            sb.append(cache.get(key));
            sb.append(",");
        }
        if (sb.lastIndexOf(",") > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        sb.append("}");
        return sb.toString();
    }
}
