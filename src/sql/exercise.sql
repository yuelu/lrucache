/**
 * Solution Assumption: all employee has salary, department and location 
 */

/**
 * 1. List the employee name, effective date and salary amount for all employees, 
 * sorting the results by location name and employee name.
 */

select e.name,effective_date,amount 
from employee e,location loc, salary s
where e.id = s.employee_id
and e.location_id = loc.id
order by loc.name, e.name;

/*
 * 2. What is the average salary for each department during 2013?
 */
select d.name, avg(amount) as average_salary
from employee e, department d, salary s
where e.department_id=d.id 
and e.id = s.employee_id 
and s.effective_date<='12/31/2013' 
and s.expiration_date>'1/1/2013' 
group by d.name;

/*
 * 3. What are the minimum and maximum salaries for each location during 2014?
 */
select loc.name, min(amount) as min_salary, max(amount) as max_salary 
from employee e, location loc, salary s
where e.location_id=loc.id 
and e.id = s.employee_id 
and s.effective_date<='12/31/2014' 
and s.expiration_date>'1/1/2014' 
group by loc.name

/*
 * 4. What is the total salary paid from 10/1/2013 to 09/30/2014?
 */
select sum(amount) as total_salary
from salary
where effective_date>='10/1/2013' 
and expiration_date<='9/30/2014' 

/*
 * 5. List the names of the employees who did not receive a pay raise in January of 2014
 */
select name
from employee 
where id not in 
(
	select distinct employee_id 
	from salary s1, salary s2
	where s1.employee_id = s2.employee_id 
	and s1.expiration_date='12/31/2013' 
	and s2.effective_date='1/1/2014'
	and s1.amount <= s2.amount
)